const items = [
  {
    name: "John",
    age: 35
  },
  {
    name: "John",
    age: 36
  },
  {
    name: "John",
    age: 37
  },
  {
    name: "John",
    age: 38
  },
  {
    name: "John",
    age: 39
  },
  {
    name: "John",
    age: 40
  },
  {
    name: "John",
    age: 41
  },
  {
    name: "John",
    age: 42
  },
  {
    name: "John",
    age: 43
  },
  {
    name: "John",
    age: 44
  },
];

export default items;
